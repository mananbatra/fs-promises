const path=require('path');
let main=require('../fs-problem1.cjs');

let numberOfFiles=`${Math.floor(Math.random() * 10)}`
let dirPath=`fileDir_${Math.floor(Math.random() * 1000)}`;

let absoluteDirPath=path.resolve(dirPath);

main(absoluteDirPath,numberOfFiles);
