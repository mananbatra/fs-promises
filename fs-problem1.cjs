const { error } = require('console');
const fs = require('fs');
const path = require('path');

function makeDir(dirPath, numberOfFiles) {                               // function to create a dir

    return new Promise((resolve, reject) => {                           // create a promise to check if dir is created then only move forward

        fs.mkdir(dirPath, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve("Directory Created Successfully");
                //createRandomFiles(dirPath, numberOfFiles);
            }
        });

    })

}

function createRandomFiles(dirPath, numberOfFiles) {
    //console.log(numberOfFiles);
    let fileArray = [];

    return new Promise((resolve, reject) => {
        for (let index = 1; index <= numberOfFiles; index++) {
            const fileName = `file_${index}.json`;
            const filePath = path.join(dirPath, fileName);

            fs.writeFile(filePath, '', (error) => {
                if (error) {
                    reject(error);
                } else {
                    fileArray.push(`file_${index}.json`);
                    console.log("file created");

                    if (fileArray.length == numberOfFiles) {
                        resolve(fileArray);
                        //deleteFiles(null, dirPath, fileArray)
                    }
                }
            });
            //console.log(fileArray.length);
        }
    })
}

function deleteFiles(error, dirPath, fileArray) {

    if (error) {
        console.error(error);
    }
    else {
        for (let index = 0; index < fileArray.length; index++) {

            const filePath = path.join(dirPath, fileArray[index])
            fs.unlink(filePath, function (error) {
                if (error) {
                    console.log(error);
                } else {
                    console.log("File deleted");
                }
            })
        }
    }
    //console.log(fileArray);
}

function main(dirPath, numberOfFiles) {
    makeDir(dirPath, numberOfFiles)
        .then((res) => {
            console.log(res);
            return createRandomFiles(dirPath, numberOfFiles);
        })
        .then((result) => {
            console.log(result);
            return deleteFiles(null, dirPath, result)
        })
        .catch((error) => {
            console.error(error);
        })
}

module.exports = main;
