const fs = require('fs');
const path = require('path');
const readFileDir = '/home/manan/Downloads'
const fileNamePath = './lipsum.txt';
const readFilePath = path.resolve(readFileDir, fileNamePath);
const dirPath = './';

const file1 = path.join(__dirname, "file1.txt");
const file2 = path.join(__dirname, "file2.txt");
const file3 = path.join(__dirname, "file3.txt");

const filePathStore = path.join(__dirname, `filenames.txt`);

//console.log();

function readData(filePath) {

    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (error, data) => {

            if (error) {
                reject(error);
            } else {
                console.log("data read successfully");
                resolve(data);
                //writeNewFile(dirPath, data)
            }
        });

    })
}

function writeNewFile(data, filePath) {

    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, JSON.stringify(data), (error) => {
            if (error) {
                reject(error);
            }
            else {
                console.log(`file created SuccessFully`);
            }
        });

        //console.log(filePath+"++++++++");
        const content = JSON.stringify(filePath) + "\n";
        //console.log(filePath)
        fs.appendFile(filePathStore, content, (error) => {
            if (error) {
                reject(error);
            }
            else {
                console.log(`filesName.txt successFully stored ${filePath}`);
                //console.log(filePath);
                resolve(filePath);
                //readAndSplit(filePath, dirPath, filePathStore);
            }
        });

    })
}

function deleteFiles(fileNameArray) {
    fileNameArray.map(file => {
        //const deleteFilePath = path.join(dirPath , JSON.parse(file));
        //console.log(deleteFilePath);
        return fs.unlink(JSON.parse(file), (err) => {
            if (err) {
                console.error(err)
            } else {
                console.log("File Successfully Deleted")
            }
        });
    })
}

function main() {
    readData(readFilePath)
        .then((data) => {
            //console.log(data);
            let upperCaseData = data.toUpperCase();
            return writeNewFile(upperCaseData, file1);
        })
            .then((filePath) => {
                console.log(filePath + "----")
                return readData(filePath)
            })
                .then((data) => {
                    let lowerCaseData = data.toLowerCase().split('\n');
                    return writeNewFile(lowerCaseData, file2);
                })
                    .then((filePath) => {
                        return readData(filePath);
                    })
                        .then((data) => {
                            let sortedData = JSON.parse(data).sort();
                            return writeNewFile(sortedData, file3);
                        })
                            .then((filePath) => {
                                return readData(filePathStore);
                            })
                                .then((data) => {
                                    let fileNameArray = data.split("\n");
                                    //console.log(fileNameArray);
                                    fileNameArray.pop();
                                    deleteFiles(fileNameArray);
                                })
                                .catch((error) => {
                                    console.error(error);
                                })


}
module.exports = main;
